//This small script will fix the priority settings from reverting to low after the loading screen.
using System;
using System.Diagnostics;

namespace Dishonored2Hotfix     {

    class Dishonored2FixPrio  {

        void FixPrio()   {
            do
            {
                //Gets a list of all running processes matching the string
                //This needs to be done instead of starting and grabbing that process since steam makes the games it runs children(?)
                Process[] localByName = Process.GetProcessesByName("Dishonored2");
                foreach (var item in localByName)
                {
                    Console.WriteLine("Found process changing priority");
                    item.PriorityClass = ProcessPriorityClass.AboveNormal;
                    //So that it doesnt kill cpu...
                    System.Threading.Thread.Sleep(10000);
                }
            } while (true);
        }

        static void Main()
        {
            Dishonored2FixPrio thing = new Dishonored2FixPrio();
            thing.FixPrio();
        }
    }
}
